# Informática e Sociedade

[TOC]

## Sobre

Esse repositório contém a ementa e conteúdo da disciplina de Informática e Sociedade da Faculdade de Computação da UFPA (ICEN) quando ministrada pelo CCSL-UFPA.

A disciplina pretende discutir os impactos das tecnologias digitais em diversos aspectos da sociedade, realizando uma análise crítica das transformações que mudanças tecnológicas implicam em setores tão diversos quanto a cultura, a economia, o trabalho, e outros.

A disciplina é dividida em módulos, conforme especificados abaixo.

## Módulos

### Criação e Distribuição da Cultura - Da Pirataria ao Streamming

Nesse módulo discutiremos como os avanços das TICs influenciaram a maneira como os produtos culturais - com especial ênfase à música - são distribuídos, como isso ocasionou a potencialização da distribuição ilegal de conteúdo (popularmente conhecido como "pirataria"), e de que maneira essas ferramentas estão sendo utilizadas para criar um novo tipo de indústria que eliminou o conceito de "posse" de produtos culturais (o "streaming").

### Plataformização e Precarização do Trabalho

Nesse módulo discutiremos como o advento das "plataformas" - empresas que subsistem intermediando a relação entre consumidores e seus objetos de consumo/necessidade, como a Uber faz com corridas privadsa e o iFood faz com restaurantes - tiveram como consequência a precarização tanto do trabalho de profissionais que desempenham esses serviços como do próprio empresariado que utiliza esses sistemas.

### Inteligência Artificial - ChatGPT

EM BREVE

## Cronograma

<table>
<tr>
    <td><b>Atividade/Conteúdo</b></td>
    <td><b>Data</b></td>
</tr>
<tr>
    <td>Apresentação da Disciplina</td>
    <td>13/03</td>
</tr>
<tr>
    <td colspan="2"><b>Módulo 1: Criação e Distribuição da Cultura - Da Pirataria ao Streamming</b></td>
</tr>
<tr>
    <td>[Good Copy Bad Copy](https://www.youtube.com/watch?v=FWSH34cnHbM)</td>
    <td>15/03</td>
</tr>
<tr>
    <td>[Downloaded](https://drive.google.com/file/d/1P4kKxxroQ-3BrnnTtIj6Ictd2nIyEXED/view?usp=sharing)</td>
    <td>20/03</td>
</tr>
<tr>
    <td>Discussão - como as TICs impactaram na pirataria?</td>
    <td>22/03</td>
</tr>
<tr>
    <td>[TPB AFK](https://www.youtube.com/watch?v=7R6SOPzjaxs)</td>
    <td>27/03</td>
</tr>
<tr>
    <td>Discussão - o impacto das TICs no compartilhamento de conteúdo cultural</td>
    <td>29/03</td>
</tr>
<tr>
    <td>[Uma Breve História da Cultura Livre](https://blog.benfeitoria.com/2015/07/28/uma-breve-historia-da-cultura-livre/) e [Brega S/A](https://www.youtube.com/watch?v=_7TBEJJCt0E)</td>
    <td>03/04</td>
</tr>
<tr>
    <td>Discussão - o mercado do Brega e a pirataria. Textos de suporte: [O mercado de streaming matou a pirataria — mas agora vai revivê-la](https://thehack.com.br/o-mercado-de-streaming-matou-a-pirataria-mas-agora-vai-revive-la/) e [Com a fragmentação do streaming, pirataria volta a ganhar força](https://manualdousuario.net/pirataria-streaming-video/); [Manifesto](https://partidopirata.org/quem-somos/estrutura-nacional/documentos/manifesto-v2-0/) e [Carta de Princípios](https://partidopirata.org/quem-somos/estrutura-nacional/documentos/carta-de-principios-v1-0/) do Partido Pirata do Brasil</td>
    <td>05/04</td>
</tr>
<tr>
    <td>Discussão - o streaming, a pirataria, e os impactos políticos da distribuição da cultura</td>
    <td>10/04</td>
</tr>
<tr>
    <td><b>Entrega do ensaio sobre o módulo</b></td>
    <td><b>12/04</b></td>
</tr>
<tr>
    <td colspan="2"><b>Módulo 2: Plataformização e Precarização do Trabalho</b></td>
</tr>
<tr>
    <td>Texto de suporte: [Uberização e plataformização do trabalho no Brasil: conceitos, processos e formas](https://www.seer.ufrgs.br/sociologias/article/view/116484) e [Até a pornografia tem: o que é uberização, como surgiu e outras dúvidas](https://tab.uol.com.br/faq/uberizacao-o-que-e-como-funciona-como-surgiu-e-outras-duvidas.htm)</td>
    <td>17/04</td>
</tr>
<tr>
    <td>[GIG - A Uberização do Trabalho](https://youtu.be/UKI8qgwPyQM) e [“A plataformização é a transformação mais radical do trabalho desde a 2ª revolução industrial”](https://www.brasildefatopr.com.br/2021/08/11/a-plataformizacao-e-a-transformacao-mais-radical-do-trabalho-desde-a-2-revolucao-industrial)</td>
    <td>19/04</td>
</tr>
<tr>
    <td>[Vidas Entregues](https://youtu.be/cT5iAJZ853c) e Discussão - o que é "uberização" e quais seus impactos nas relações de trabalho?</td>
    <td>24/04</td>
</tr>
<tr>
    <td>[Como apps de entrega estão levando pequenos restaurantes à falência](https://www.bbc.com/portuguese/geral-51272233) e ['Dark kitchens': o que são as 'cozinhas fantasma', que só existem em apps de comida](https://www.bbc.com/portuguese/internacional-51624844)</td>
    <td>26/04</td>
</tr>
<tr>
    <td><b>FERIADO</b></td>
    <td><b>01/05</b></td>
</tr>
<tr>
    <td>Discussão - qual o impacto da "uberização" para os empresários? Há saídas para essa situação?</td>
    <td>03/05</td>
</tr>
<tr>
    <td>Entrega do ensaio sobre o módulo</td>
    <td>08/05</td>
</tr>
<tr>
    <td colspan="2"><b>Módulo 3: Fake News</b></td>
</tr>
<tr>
    <td><i>Depois da Verdade: Desinformação e o Custo das Fake News</i> (HBO)</td>
    <td>10/05</td>
</tr>
<tr>
    <td>Artigo de suporte [O fenômeno das fake news: definição, combate e contexto](https://revista.internetlab.org.br/o-fenomeno-das-fake-news-definicao-combate-e-contexto/)</td>
    <td>15/05</td>
</tr>
<tr>
    <td>Discussão - o que são as fake news e seus impactos</td>
    <td>17/05</td>
</tr>
</table>
